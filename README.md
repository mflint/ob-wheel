ob-wheel
========

An SVG obstetrics wheel... otherwise known as pregnancy calculator or calendar.

Instructions for use
--------------------
1. Print two copies
2. Cut around the outer dotted lines on one copy
3. Cut around the inner dotted lines on the second copy
4. Profit!

